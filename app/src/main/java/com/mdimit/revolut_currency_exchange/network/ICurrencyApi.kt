package com.mdimit.revolut_currency_exchange.network

import com.mdimit.revolut_currency_exchange.model.network.CurrencyRatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ICurrencyApi {

    @GET("latest")
    fun currencyRates(@Query("base") baseCurrency: String): Single<CurrencyRatesResponse>
}