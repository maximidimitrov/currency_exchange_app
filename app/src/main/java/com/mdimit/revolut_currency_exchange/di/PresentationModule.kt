package com.mdimit.revolut_currency_exchange.di

import com.mdimit.revolut_currency_exchange.model.ui.CurrencyRowUiModel
import com.mdimit.revolut_currency_exchange.presentation.adapter.CountryResourceMapper
import com.mdimit.revolut_currency_exchange.presentation.adapter.CurrencyExchangeAdapter
import com.mdimit.revolut_currency_exchange.presentation.adapter.CurrencyExchangeDiffCallback
import com.mdimit.revolut_currency_exchange.presentation.viewmodel.CurrencyExchangeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    factory { (onCurrencySelected: (CurrencyRowUiModel) -> Unit, onValueChanged: (Double) -> Unit) ->
        CurrencyExchangeAdapter(onCurrencySelected, onValueChanged, get(), get())
    }
    viewModel { CurrencyExchangeViewModel(get()) }
    factory { CurrencyExchangeDiffCallback() }
    single { CountryResourceMapper() }
}