package com.mdimit.revolut_currency_exchange.di

import com.mdimit.revolut_currency_exchange.domain.mapper.CurrencyRatesResponseMapper
import com.mdimit.revolut_currency_exchange.domain.mapper.MergeBaseRateMapper
import com.mdimit.revolut_currency_exchange.domain.repository.CurrencyRepository
import com.mdimit.revolut_currency_exchange.domain.usecase.CurrencyRatesUseCase
import org.koin.dsl.module

val domainModule = module {
    single { CurrencyRatesResponseMapper() }
    single { CurrencyRepository(get(), get()) }
    single {CurrencyRatesUseCase(get())}
}