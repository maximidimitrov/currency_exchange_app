package com.mdimit.revolut_currency_exchange.domain.mapper

import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRate
import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRates

class MergeBaseRateMapper {

    fun map(currencyRates: CurrencyRates): Map<String, CurrencyRate> {
        return currencyRates.let {
            HashMap<String, CurrencyRate>().apply {
                put(it.baseCurrency.key, it.baseCurrency)
                putAll(it.rates)
            }
        }
    }
}