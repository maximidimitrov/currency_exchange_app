package com.mdimit.revolut_currency_exchange.domain.repository

import com.mdimit.revolut_currency_exchange.domain.mapper.CurrencyRatesResponseMapper
import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRates
import com.mdimit.revolut_currency_exchange.network.ICurrencyApi
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class CurrencyRepository(
    private val currencyApi: ICurrencyApi,
    private val responseMapper: CurrencyRatesResponseMapper
) {

    fun getRates(baseCurrency: String): Single<CurrencyRates> {
        return currencyApi.currencyRates(baseCurrency).map { responseMapper.map(it) }
    }
}