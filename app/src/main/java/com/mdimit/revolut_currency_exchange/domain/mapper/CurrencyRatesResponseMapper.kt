package com.mdimit.revolut_currency_exchange.domain.mapper

import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRate
import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRates
import com.mdimit.revolut_currency_exchange.model.network.CurrencyRatesResponse

class CurrencyRatesResponseMapper {

    fun map(currencyRatesResponse: CurrencyRatesResponse): CurrencyRates =
        with(currencyRatesResponse) {
            CurrencyRates(
                CurrencyRate(baseCurrency, 1.0),
                rates.mapValues { CurrencyRate(it.key, it.value) }
            )
        }
}