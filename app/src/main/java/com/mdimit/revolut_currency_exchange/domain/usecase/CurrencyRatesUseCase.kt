package com.mdimit.revolut_currency_exchange.domain.usecase

import com.mdimit.revolut_currency_exchange.domain.repository.CurrencyRepository
import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRates
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class CurrencyRatesUseCase(
    private val currencyRepository: CurrencyRepository
) {

    fun getCurrencyRates(
        baseCurrencyChange: Flowable<String>
    ): Flowable<CurrencyRates> {
        return Flowable.combineLatest(
            baseCurrencyChange.observeOn(Schedulers.io()),
            Flowable.interval(0, 1, TimeUnit.SECONDS),
            BiFunction<String, Long, String?> { baseCurrency, _ -> baseCurrency })
            .subscribeOn(Schedulers.io())
            .flatMap {
                currencyRepository.getRates(it).toFlowable()
            }
    }
}