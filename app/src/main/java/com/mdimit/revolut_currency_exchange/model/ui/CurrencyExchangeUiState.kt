package com.mdimit.revolut_currency_exchange.model.ui

sealed class CurrencyExchangeUiState {
    object Loading : CurrencyExchangeUiState()
    object Error : CurrencyExchangeUiState()
    data class Ready(val data: List<CurrencyRowUiModel>): CurrencyExchangeUiState()
}