package com.mdimit.revolut_currency_exchange.model.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class CountryResource(@StringRes val name: Int, @DrawableRes val flag: Int)
