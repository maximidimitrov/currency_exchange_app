package com.mdimit.revolut_currency_exchange.model.network

import com.squareup.moshi.Json

data class CurrencyRatesResponse(
    @Json(name = "baseCurrency") val baseCurrency: String,
    @Json(name = "rates") val rates: Map<String, Double>
)