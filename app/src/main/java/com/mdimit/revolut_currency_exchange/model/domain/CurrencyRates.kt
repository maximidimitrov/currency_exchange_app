package com.mdimit.revolut_currency_exchange.model.domain

data class CurrencyRates(val baseCurrency: CurrencyRate, val rates: Map<String, CurrencyRate>)

data class CurrencyRate(val key: String, val rate: Double)