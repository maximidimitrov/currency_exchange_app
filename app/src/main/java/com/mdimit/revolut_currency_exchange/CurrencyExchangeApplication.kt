package com.mdimit.revolut_currency_exchange

import android.app.Application
import com.mdimit.revolut_currency_exchange.di.domainModule
import com.mdimit.revolut_currency_exchange.di.networkModule
import com.mdimit.revolut_currency_exchange.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CurrencyExchangeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@CurrencyExchangeApplication)
            modules(
                networkModule,
                domainModule,
                presentationModule
            )
        }
    }
}