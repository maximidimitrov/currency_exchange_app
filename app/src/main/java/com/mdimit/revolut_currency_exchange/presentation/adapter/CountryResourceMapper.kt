package com.mdimit.revolut_currency_exchange.presentation.adapter

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.mdimit.revolut_currency_exchange.R
import com.mdimit.revolut_currency_exchange.model.ui.CountryResource

class CountryResourceMapper {

    fun toCountryResource(isoCode: String): CountryResource? = when (isoCode.toLowerCase()) {
        "eur" -> CountryResource(R.string.eur, R.drawable.eu)
        "aud" -> CountryResource(R.string.aud, R.drawable.australia)
        "bgn" -> CountryResource(R.string.bgn, R.drawable.bulgaria)
        "brl" -> CountryResource(R.string.brl, R.drawable.brazil)
        "cad" -> CountryResource(R.string.cad, R.drawable.canada)
        "chf" -> CountryResource(R.string.chf, R.drawable.switzerland)
        "cny" -> CountryResource(R.string.cny, R.drawable.china)
        "czk" -> CountryResource(R.string.czk, R.drawable.czech_republic)
        "dkk" -> CountryResource(R.string.dkk, R.drawable.denmark)
        "gbp" -> CountryResource(R.string.gbp, R.drawable.uk)
        "hkd" -> CountryResource(R.string.hkd, R.drawable.hong_kong)
        "hrk" -> CountryResource(R.string.hrk, R.drawable.croatia)
        "huf" -> CountryResource(R.string.huf, R.drawable.hungary)
        "idr" -> CountryResource(R.string.idr, R.drawable.indonesia)
        "ils" -> CountryResource(R.string.ils, R.drawable.israel)
        "inr" -> CountryResource(R.string.inr, R.drawable.india)
        "isk" -> CountryResource(R.string.isk, R.drawable.iceland)
        "jpy" -> CountryResource(R.string.jpy, R.drawable.japan)
        "krw" -> CountryResource(R.string.krw, R.drawable.south_korea)
        "mxn" -> CountryResource(R.string.mxn, R.drawable.mexico)
        "myr" -> CountryResource(R.string.myr, R.drawable.malaysia)
        "nok" -> CountryResource(R.string.nok, R.drawable.norway)
        "nzd" -> CountryResource(R.string.nzd, R.drawable.new_zeland)
        "php" -> CountryResource(R.string.php, R.drawable.philippines)
        "pln" -> CountryResource(R.string.pln, R.drawable.poland)
        "ron" -> CountryResource(R.string.ron, R.drawable.romania)
        "rub" -> CountryResource(R.string.rub, R.drawable.russia)
        "sek" -> CountryResource(R.string.sek, R.drawable.sweden)
        "sgd" -> CountryResource(R.string.sgd, R.drawable.singapore)
        "thb" -> CountryResource(R.string.thb, R.drawable.thailand)
        "usd" -> CountryResource(R.string.usd, R.drawable.usa)
        "zar" -> CountryResource(R.string.zar, R.drawable.south_africa)
        else -> null
    }
}