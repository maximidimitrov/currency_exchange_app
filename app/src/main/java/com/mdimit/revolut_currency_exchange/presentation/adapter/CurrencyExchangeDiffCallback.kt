package com.mdimit.revolut_currency_exchange.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.mdimit.revolut_currency_exchange.model.ui.CurrencyRowUiModel

class CurrencyExchangeDiffCallback: DiffUtil.ItemCallback<CurrencyRowUiModel>() {

    override fun areItemsTheSame(
        oldItem: CurrencyRowUiModel,
        newItem: CurrencyRowUiModel
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: CurrencyRowUiModel,
        newItem: CurrencyRowUiModel
    ): Boolean {
        return "%.2f".format(oldItem.value) == "%.2f".format(newItem.value)
    }
}