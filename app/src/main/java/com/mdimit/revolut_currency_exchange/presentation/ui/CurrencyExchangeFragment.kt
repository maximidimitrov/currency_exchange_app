package com.mdimit.revolut_currency_exchange.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.mdimit.revolut_currency_exchange.R
import com.mdimit.revolut_currency_exchange.model.ui.CurrencyExchangeUiState
import com.mdimit.revolut_currency_exchange.model.ui.CurrencyRowUiModel
import com.mdimit.revolut_currency_exchange.presentation.adapter.CurrencyExchangeAdapter
import com.mdimit.revolut_currency_exchange.presentation.viewmodel.CurrencyExchangeViewModel
import kotlinx.android.synthetic.main.fragment_currency_exchange.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CurrencyExchangeFragment : Fragment() {

    private val viewModel: CurrencyExchangeViewModel by viewModel()

    private val currencyAdapter: CurrencyExchangeAdapter by inject {
        parametersOf(
            viewModel::onCurrencySelected,
            viewModel::onValueChange
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_currency_exchange, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        viewModel.currenciesUiState.observe(viewLifecycleOwner, Observer {
            when(it) {
                CurrencyExchangeUiState.Loading -> showLoading()
                CurrencyExchangeUiState.Error -> showError()
                is CurrencyExchangeUiState.Ready -> showCurrencies(it.data)
            }
        })
    }

    private fun showCurrencies(data: List<CurrencyRowUiModel>) {
        currencyAdapter.data = data
        progressBar.visibility = View.GONE
        currencyRv.visibility = View.VISIBLE
    }

    private fun showLoading() {
        progressBar.visibility = View.VISIBLE
        currencyRv.visibility = View.GONE
    }

    private fun showError() {
        Toast.makeText(context,"Something went wrong", Toast.LENGTH_LONG).show()
    }

    private fun initRecyclerView() {
        currencyRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = currencyAdapter
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
    }
}