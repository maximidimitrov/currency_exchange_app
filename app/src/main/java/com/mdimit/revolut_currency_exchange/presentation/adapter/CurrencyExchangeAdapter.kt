package com.mdimit.revolut_currency_exchange.presentation.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.mdimit.revolut_currency_exchange.R
import com.mdimit.revolut_currency_exchange.model.ui.CurrencyRowUiModel
import kotlinx.android.synthetic.main.item_currency_exchange.view.*

class CurrencyExchangeAdapter(
    private val onCurrencySelected: (CurrencyRowUiModel) -> Unit,
    private val onValueChanged: (Double) -> Unit,
    private val countryResourceMapper: CountryResourceMapper,
    diffCallback: CurrencyExchangeDiffCallback
) : RecyclerView.Adapter<CurrencyExchangeAdapter.CurrencyRowViewHolder>() {

    private val currencyTextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
            if (text != null) {
                onValueChanged(text.toString().toDoubleOrNull() ?: 0.0)
            }
        }
    }

    private val diff = AsyncListDiffer(this, diffCallback)

    var data: List<CurrencyRowUiModel>
        get() = diff.currentList
        set(value) = diff.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyRowViewHolder {
        return CurrencyRowViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_currency_exchange, parent, false)
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: CurrencyRowViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class CurrencyRowViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val iso: TextView = view.isoTv
        private val amount: EditText = view.amountEt
        private val flagImage: ImageView = view.flagImage
        private val currencyName: TextView = view.currencyNameTv

        init {
            amount.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onCurrencySelected(data[adapterPosition])
                    amount.setSelection(amount.text.length)
                    amount.addTextChangedListener(currencyTextWatcher)
                } else {
                    amount.removeTextChangedListener(currencyTextWatcher)
                }
            }
            view.setOnClickListener {
                if (adapterPosition != 0) {
                    amount.requestFocus()
                }
            }
        }

        fun bind(data: CurrencyRowUiModel) {
            iso.text = data.name
            countryResourceMapper.toCountryResource(data.name)?.let {
                currencyName.setText(it.name)
                flagImage.setImageResource(it.flag)
            }
            if (!amount.isFocused) {
                amount.setText("%.2f".format(data.value))
            }
        }
    }
}