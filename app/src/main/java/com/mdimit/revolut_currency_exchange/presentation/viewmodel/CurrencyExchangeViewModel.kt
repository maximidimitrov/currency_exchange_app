package com.mdimit.revolut_currency_exchange.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mdimit.revolut_currency_exchange.domain.usecase.CurrencyRatesUseCase
import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRate
import com.mdimit.revolut_currency_exchange.model.domain.CurrencyRates
import com.mdimit.revolut_currency_exchange.model.ui.CurrencyExchangeUiState
import com.mdimit.revolut_currency_exchange.model.ui.CurrencyRowUiModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers

class CurrencyExchangeViewModel(
    currencyRatesUseCase: CurrencyRatesUseCase
) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val currencySelection: BehaviorProcessor<String> = BehaviorProcessor.create()
    private var baseCurrency: CurrencyRowUiModel = CurrencyRowUiModel(DEFAULT_CURRENCY_KEY, 100.0)
        set(value) {
            if (field.name != value.name) {
                currencySelection.onNext(value.name)
            }
            field = value
        }
    private var currentRates: Map<String, CurrencyRate>? = null

    private var currencyList: List<CurrencyRowUiModel> = emptyList()
        set(value) {
            if (value.isNotEmpty()) {
                _currenciesUiState.postValue(CurrencyExchangeUiState.Ready(value))
            }
            field = value
        }

    private val _currenciesUiState = MutableLiveData<CurrencyExchangeUiState>()
    val currenciesUiState = _currenciesUiState

    init {
        disposables.add(
            currencyRatesUseCase.getCurrencyRates(currencySelection)
                .doOnSubscribe {
                    _currenciesUiState.postValue(CurrencyExchangeUiState.Loading)
                    currencySelection.onNext(baseCurrency.name)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(::onRatesChange, ::onError)
        )
    }

    private fun onRatesChange(currencyRates: CurrencyRates) {
        if (currencyRates.baseCurrency.key != baseCurrency.name) return

        val rates = createCurrentRates(currencyRates)
        val recalculated = if (currencyList.isEmpty()) {
            initializeCurrenciesUiData(currencyRates)
        } else {
            recalculate(rates)
        }
        currentRates = rates
        currencyList = recalculated
    }

    private fun initializeCurrenciesUiData(currencyRates: CurrencyRates): List<CurrencyRowUiModel> {
        val rates = currencyRates.rates.values.sortedBy { it.key }
            .map { CurrencyRowUiModel(it.key, it.rate * baseCurrency.value) }
        return mutableListOf<CurrencyRowUiModel>().apply {
            add(baseCurrency)
            addAll(rates)
        }
    }

    private fun recalculate(rates: Map<String, CurrencyRate>): List<CurrencyRowUiModel> {
        return currencyList.map {
            val rate = rates[it.name]?.rate ?: 0.0
            it.copy(value = rate * baseCurrency.value)
        }
    }

    private fun createCurrentRates(currencyRates: CurrencyRates): Map<String, CurrencyRate> {
        return currencyRates.rates.toMutableMap().apply {
            put(currencyRates.baseCurrency.key, currencyRates.baseCurrency)
        }
    }

    private fun onError(error: Throwable) {
        _currenciesUiState.value = CurrencyExchangeUiState.Error
    }

    fun onValueChange(newValue: Double) {
        baseCurrency = baseCurrency.copy(value = newValue)
        currentRates?.let {
            recalculate(it)
        }
    }

    fun onCurrencySelected(currency: CurrencyRowUiModel) {
        moveSelectedToTop(currency)
        baseCurrency = currency
    }

    private fun moveSelectedToTop(currency: CurrencyRowUiModel) {
        currencyList = currencyList.toMutableList().apply {
            removeAt(indexOf(currency))
            add(0, currency)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    companion object {
        private const val DEFAULT_CURRENCY_KEY = "EUR"
    }
}